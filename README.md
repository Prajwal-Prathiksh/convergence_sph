# Second order convergent weakly-compressible SPH schemes

This repository contains the code and manuscript for research done on a study
of the convergence of the SPH method for incompressible fluids wherein we
introduce a new family of weakly-compressible SPH schemes.

The full paper is available at: https://arxiv.org/abs/2107.11859

All the results presented therein may be reproduced by following the
instructions below.

## Installation

This requires pysph to be setup along with automan. See the
`requirements.txt`. To setup perform the following:

0. Setup a suitable Python distribution, using
   [edm](https://docs.enthought.com/edm/) or [conda](https://conda.io) or a
   [virtualenv](https://virtualenv.pypa.io/).

1. Clone this repository:
```
    $ git clone https://gitlab.com/prabhu/convergence_sph.git
```

2. Run the following from your Python environment:
```
    $ cd convergence_sph
    $ pip install -r requirements.txt
```


## Generating the results

The paper and the results are all automated using the
[automan](https://automan.readthedocs.io) package which should already be
installed as part of the above installation. This will perform all the
required simulations (this can take a while) and also generate all the plots
for the manuscript.

To use the automation code, do the following::

    $ python automate.py
    # or
    $ ./automate.py

By default the simulation outputs are in the ``outputs`` directory and the
final plots for the paper are in ``manuscript/figures``.


## Building the paper

The manuscript is written with LaTeX and if you have that installed you may do
the following:

```
$ cd manuscript
$ pdflatex convergence_sph_2.tex
$ bibtex convergence_sph_2
$ pdflatex convergence_sph_2.tex
$ pdflatex convergence_sph_2.tex
```
