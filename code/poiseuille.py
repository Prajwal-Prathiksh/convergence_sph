'''Hagen-Poiseulle flow
The flow in a circular pipe with no-slip boundaries
'''
import os
import numpy as np

from pysph.solver.application import Application
from pysph.base.nnps import DomainManager
from pysph.base.utils import get_particle_array
from pysph.sph.equation import Group, Equation
from pysph.solver.utils import load

# Numerical setup
U = 1.0
d = 0.5
Ly = 2*d
Lx = 0.4*Ly
rho0 = 1.0
c0 = 10
p0 = rho0*c0**2
nu = 0.1

class PoiseuilleFlow(Application):
    def initialize(self):
        self.interp = None

    def add_user_options(self, group):
        group.add_argument(
            "--nx", action="store", type=int, dest="nx", default=50,
            help="Number of points along x direction. (default 50)"
        )
        group.add_argument(
            "--re", action="store", type=float, dest="re", default=10.,
            help="Reynolds number (defaults to 10)."
        )
        group.add_argument(
            "--hdx", action="store", type=float, dest="hdx", default=1.2,
            help="Ratio h/dx."
        )
        group.add_argument(
            "--pb-factor", action="store", type=float, dest="pb_factor",
            default=1.0,
            help="Use fraction of the background pressure (default: 1.0)."
        )
        group.add_argument(
            "--c0-fac", action="store", type=float, dest="c0_fac",
            default=10.0,
            help="default factor is 10"
        )
        corrections = ['', 'mixed', 'gradient', 'crksph', 'kgf', 'order1']
        group.add_argument(
            "--kernel-correction", action="store", type=str,
            dest='kernel_correction',
            default='', help="Type of Kernel Correction", choices=corrections
        )
        group.add_argument(
            "--remesh", action="store", type=int, dest="remesh", default=0,
            help="Remeshing frequency (setting it to zero disables it)."
        )
        remesh_types = ['m4', 'sph']
        group.add_argument(
            "--remesh-eq", action="store", type=str, dest="remesh_eq",
            default='m4', choices=remesh_types,
            help="Remeshing strategy to use."
        )
        group.add_argument(
            "--shift-freq", action="store", type=int, dest="shift_freq",
            default=0,
            help="Particle position shift frequency.(set zero to disable)."
        )
        shift_types = ['simple', 'fickian', 'ipst', 'mod_fickian', 'delta_plus']
        group.add_argument(
            "--shift-kind", action="store", type=str, dest="shift_kind",
            default='simple', choices=shift_types,
            help="Use of fickian shift in positions."
        )
        group.add_argument(
            "--shift-parameter", action="store", type=float,
            dest="shift_parameter", default=None,
            help="Constant used in shift, range for 'simple' is 0.01-0.1"
            "range 'fickian' is 1-10."
        )
        group.add_argument(
            "--shift-correct-vel", action="store_true",
            dest="correct_vel", default=False,
            help="Correct velocities after shifting (defaults to false)."
        )
        group.add_argument(
            "--no-periodic", action="store_false",
            dest="no_periodic",
            help="Make periodic domain"
        )


    def consume_user_options(self):
        nx = self.options.nx
        re = self.options.re

        self.c0 = c0
        self.nu = nu

        self.dx = dx = Ly / nx
        self.volume = dx * dx
        self.hdx = self.options.hdx

        h0 = self.hdx * self.dx
        self.fx = U * 2*self.nu/(d**2)
        if self.options.scheme.endswith('isph'):
            dt_cfl = 0.25 * dx / U
        else:
            dt_cfl = 0.25 * dx / (self.c0 + U)
        dt_viscous = 0.125 * dx**2 / nu
        dt_force = 0.25 * np.sqrt(h0/self.fx)

        self.dt = min(dt_cfl, dt_viscous, dt_force)
        self.tf = 25.0
        self.kernel_correction = self.options.kernel_correction
        self.no_periodic = self.options.no_periodic

    def pre_step(self, solver):
        from tg_config import prestep
        from pysph.tools.interpolator import Interpolator
        prestep(self, solver)
        if self.options.scheme == 'tsph':
            fluid = self.particles[0]
            if solver.t < 100.0:
                x = fluid.x
                y = fluid.y
                dx = self.dx
                cond = (y<3*dx) | (y>1.0-3*dx)
                x = x[cond]
                y = y[cond]
                if self.interp is None:
                    self.interp = Interpolator(self.particles, method='order1')

                self.interp.set_interpolation_points(x=x, y=y)
                u = self.interp.interpolate('u')
                fluid.u[cond] = u

    def post_step(self, solver):
        if self.options.scheme == 'tsph' or self.options.scheme == 'tisph':
            if self.options.remesh == 0:
                self.scheme.scheme.post_step(self.particles, self.domain)

    def configure_scheme(self):
        from tg_config import configure_scheme
        configure_scheme(self, p0, gx=self.fx)

    def create_scheme(self):
        from tg_config import create_scheme
        return create_scheme(rho0, c0, p0, solids=['channel'])

    def create_equations(self):
        from tg_config import create_equation
        return create_equation(self, solids=['channel'])

    def create_domain(self):
        if self.options.no_periodic:
            return DomainManager(
                xmin=0, xmax=Lx, periodic_in_x=True,
            )

    def create_particles(self):
        dx = self.dx
        nl = 5 * dx

        _x = np.arange( dx/2, Lx, dx )
        _y = np.arange( dx/2, Ly, dx )
        x, y = np.meshgrid(_x, _y)

        fx = x.ravel(); fy = y.ravel()

        # create the channel particles at the top
        _x = np.arange( dx/2, Lx, dx )
        _y = np.arange(Ly+dx/2, Ly+dx/2+nl, dx)
        x, y = np.meshgrid(_x, _y); tx = x.ravel(); ty = y.ravel()

        # create the channel particles at the bottom
        _y = np.arange(-dx/2, -dx/2-nl, -dx)
        x, y = np.meshgrid(_x, _y); bx = x.ravel(); by = y.ravel()

        # concatenate the top and bottom arrays
        cx = np.concatenate( (tx, bx) )
        cy = np.concatenate( (ty, by) )

        m = self.volume * rho0
        V0 = self.volume
        h = self.hdx * dx
        # create the arrays
        channel = get_particle_array(
            name='channel', x=cx, y=cy, m=m, h=h, rhoc=rho0, V=1/V0, V0=V0,
            rho=rho0)
        fluid = get_particle_array(
            name='fluid', x=fx, y=fy, m=m, h=h, rhoc=rho0, V=1/V0, V0=V0,
            rho=rho0)

        print("Poiseuille flow :: Re = 0.0125, nfluid = %d, nchannel=%d"%(
            fluid.get_number_of_particles(),
            channel.get_number_of_particles()))

        # add requisite properties to the arrays:
        self.scheme.setup_properties([fluid, channel])

        nfp = fluid.get_number_of_particles()
        fluid.gid[:] = np.arange(nfp)
        fluid.add_constant('c0', self.c0)
        fluid.add_property('rhoc')
        fluid.add_property('T')
        nfp = channel.get_number_of_particles()
        channel.gid[:] = np.arange(nfp)
        channel.add_constant('c0', self.c0)
        channel.add_property('rhoc')
        channel.add_property('T')

        channel.add_property('normal', stride=3)
        cond = cy > 0.5
        yn = np.ones_like(cy)
        yn[cond] = -1
        channel.normal[1::3] = yn

        return [fluid, channel]


    def post_process(self, info_fname):
        info = self.read_info(info_fname)
        if len(self.output_files) == 0:
            return

        import matplotlib
        matplotlib.use('agg')

        u_ex, y, u = self._plot_u_vs_y()
        t, ke = self._plot_ke_history()
        res = os.path.join(self.output_dir, "results.npz")
        np.savez(res, t=t, ke=ke, y=y, u=u, u_ex=u_ex)

    def _plot_ke_history(self):
        from pysph.tools.pprocess import get_ke_history
        from matplotlib import pyplot as plt
        t, ke = get_ke_history(self.output_files, 'fluid')
        plt.clf()
        plt.plot(t, ke)
        plt.xlabel('t')
        plt.ylabel('kinetic energy')
        fig = os.path.join(self.output_dir, "ke_history.png")
        plt.savefig(fig, dpi=300)
        return t, ke

    def _plot_u_vs_y(self):
        files = self.output_files

        # take the last solution data
        fname = files[-1]
        data = load(fname)
        tf = data['solver_data']['t']
        fluid = data['arrays']['fluid']
        u = fluid.u.copy()
        y = fluid.y.copy()

        # exact parabolic profile for the u-velocity
        fx = self.fx
        nu = self.nu

        # ye = np.linspace(0, Ly, 100)
        ue = 0.5 * fx/nu * y*(Ly - y)
        from matplotlib import pyplot as plt
        plt.clf()
        plt.plot(y, ue, label="exact")
        plt.plot(y, u, 'ko', fillstyle='none', label="computed")
        plt.xlabel('y')
        plt.ylabel('u')
        plt.legend()
        plt.title('velocity profile at %s'%tf)
        fig = os.path.join(self.output_dir, "comparison.png")
        plt.savefig(fig, dpi=300)
        return ue, y, u


if __name__ == '__main__':
    app = PoiseuilleFlow()
    app.run()
    app.post_process(app.info_filename)

