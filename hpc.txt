#!/bin/bash
#PBS -N convergence_sph
#PBS -q big
#PBS -l select=1:ncpus=32
#PBS -j oe
#PBS -V
#PBS -o log_convergence_sph.out

cd $PBS_O_WORKDIR
cat $PBS_NODEFILE > ./pbsnodes_script
PROCS1=$(cat $PBS_NODEFILE | wc -l)

python automate.py TaylorGreenVarTimestep -f
