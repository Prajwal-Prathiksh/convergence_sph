#!/usr/bin/env python
import os
import glob
import json

from itertools import cycle, product
from automan.api import PySPHProblem
from automan.api import Problem
from automan.api import Automator, Simulation, filter_cases
from automan.api import mdict, dprod, opts2path
import numpy as np
import matplotlib
matplotlib.use('pdf')
from matplotlib import pyplot as plt


params = {'legend.fontsize': 'X-large',
         'axes.labelsize': 'X-large',
         'axes.titlesize':'X-large',
         'xtick.labelsize':'X-large',
         'ytick.labelsize':'X-large'}
plt.rcParams.update(params)

datadir = os.path.join('data')

LINESTYLE = [
    (0, (1, 1)),
    (0, (5, 5)),
    (0, (3, 5, 1, 5)),
    (0, (3, 5, 1, 5, 1, 5)),
    (0, (1, 1)),
    (0, (5, 1)),
    (0, (3, 1, 1, 1)),
    (0, (3, 1, 1, 1, 1, 1))
]

MARKER = [
    '*', 'o', '<', '+', '^', 'x', '>', '.'
]

CLEAN = False
BASE_HDX = 1.2
BASE_N = 100
# RESOLUTIONS = [50, 100, 200, 250, 400, 500, 800, 1000]
RESOLUTIONS = [50, 100, 200, 250, 400, 500]

import matplotlib.font_manager as font_manager

def get_all_unique_values(list_of_dicts, key, option=None):
    if option is None:
        res = list(set([d[key] for d in list_of_dicts]))
        res.sort()
    else:
        temp_list = []
        for k, v in option.items():
            valids = [d for d in list_of_dicts if d[k] == v]
            # Add each valid to temp_list if not already there
            for d in valids:
                if d not in temp_list:
                    temp_list.append(d)
        # Remove duplicates
        print(temp_list)
        res = list(set([d[key] for d in temp_list]))
        res.sort()
    return res

def reset_cycle(cycle_obj, list):
    while next(cycle_obj) != list[-1]:
        1

def get_kernel_label(kernel):
    if kernel == 'Gaussian':
        return 'G'
    elif kernel == 'CubicSpline':
        return 'CS'
    elif kernel == 'QuinticSpline':
        return 'QS'
    elif kernel == 'WendlandQuintic':
        return r'$WQ_{2}$'
    elif kernel == 'WendlandQuinticC4':
        return r'$WQ_{4}$'
    elif kernel == 'WendlandQuinticC6':
        return r'$WQ_{6}$'

def get_cpu_time(case):
    info = glob.glob(case.input_path('*.info'))[0]
    with open(info) as fp:
        data = json.load(fp)
    return round(data['cpu_time'], 2)

def get_label_from_scheme(scheme):
    if "tvf"==scheme:
        return 'TVF'
    elif 'ewcsph' ==scheme:
        return 'EWCSPH'
    elif 'delta_plus' ==scheme:
        return r'$\delta^{+}SPH$'
    elif 'tsph'==scheme:
        return 'L-IPST-C'
    elif 'edac' ==scheme:
        return 'EDAC'

def make_table(column_names, row_data, output_fname, sort_cols=None,
               multicol=None, **extra_kw):
    import pandas as pd
    col_data = [[] for i in range(len(column_names))]
    for row in row_data:
        for i, item in enumerate(row):
            col_data[i].append(item)
    d = {n: col_data[i] for i, n in enumerate(column_names)}
    df = pd.DataFrame(d, columns=column_names)
    if sort_cols:
        sort_by = [column_names[i] for i in sort_cols]
    else:
        sort_by = column_names[-1]
    df.sort_values(by=sort_by, inplace=True)
    parent = os.path.dirname(output_fname)
    if not os.path.exists(parent):
        os.mkdir(parent)
    with open(output_fname, 'w') as fp:
        fp.write(df.to_latex(index=False, escape=False, **extra_kw))
        fp.close()
    if multicol is not None:
        import fileinput
        for line in fileinput.FileInput(output_fname, inplace=1):
            if "\\toprule" in line:
                line = line.replace(line, multicol)
            print(line, end='')


def scheme_opts(params):
    if isinstance(params, tuple):
        return params[0]
    else:
        return params


class SPHApproxBase(PySPHProblem):
    def get_name(self):
        return 'sph_approx'

    def _get_file(self):
        return 'code/approximations.py --openmp'

    def get_data_name(self, method, kernel, derv, N, pert, hdx, err=5e-4):
        return f'{method}_{kernel}_{N}_d{derv}_p{pert}_{hdx:.2f}_{err:.6f}'

    def get_data_name_for_order(self, method, kernel, derv, pert):
        return f'{method}_{kernel}_d{derv}_p{pert}'

    def read_data(self):
        self.data = {}
        for case in self.cases:
            method = case.params['use_sph']
            kernel = case.params['kernel']
            derv = case.params['derv']
            N = case.params['N']
            pert = case.params['perturb']
            hdx = case.params['hdx']
            err = case.params['max_err']
            name = self.get_data_name(method, kernel, derv, N, pert, hdx=hdx, err=err)
            print(name, hdx)
            data = np.load(self.input_path(name, 'results.npz'), mmap_mode='r')
            cpu_time = get_cpu_time(case)
            fe = data['fe']
            fc = data['fc']
            mom = sum(fc)/max(abs(fc))
            l2 = np.sqrt(sum((fe-fc)**2)/len(fe))
            # l1 = sum(abs(fe-fc))/len(fe)
            if sum(abs(fe)) > 1e-14:
                l1 = sum(abs(fe-fc))/sum(abs(fe))
            else:
                l1 = sum(abs(fe-fc))/len(fe)
            linf = max(abs(fe-fc))
            self.data[name] = {}
            self.data[name]['l2'] = l2
            self.data[name]['l1'] = l1
            self.data[name]['linf'] = linf
            self.data[name]['t'] = cpu_time
            self.data[name]['F'] = mom

class RemeshingEquations(Problem):
    def get_name(self):
        return 'remesh_eq_compare'

    def setup(self):
        pass

    def run(self):
        self.make_output_dir()
        self.plot_hdx_1()
        self.plot_hdx_1_5()

    def plot_hdx_1(self):
        import code.remesh as remesh
        import matplotlib.pyplot as plt
        g = remesh.QuinticSpline(dim=2)
        l3_eqs = [remesh.Lambda3(dest='interpolate', sources=['src'])]
        m4_eqs = [remesh.M4(dest='interpolate', sources=['src'])]

        rg_m = remesh.moment_errors(g, 1.0)
        rl3_m = remesh.moment_errors(g, 1.0, equations=l3_eqs)
        rm4_m = remesh.moment_errors(g, 1.0, equations=m4_eqs)

        rg = remesh.find_error(g, 1.0)
        rl3 = remesh.find_error(g, 1.0, equations=l3_eqs)
        rm4 = remesh.find_error(g, 1.0, equations=m4_eqs)

        m = 'm2'
        # plt.figure(figsize=(5, 5))
        plt.loglog(rg_m['sizes'], rg_m['moment'][m], '--', label='SPH')
        plt.loglog(rl3_m['sizes'], rl3_m['moment'][m], '.-', label=r'$\Lambda_3$')
        plt.loglog(rm4_m['sizes'], rm4_m['moment'][m], ':', label=r'$M_4$')
        plt.legend()
        plt.grid()
        plt.xlabel('N')
        plt.ylabel(r'$M_2$')
        plt.title(r'$\sum u (x^2 + y^2) dx^2)$')

        plotfile = self.output_path('hdx1.png')
        plt.savefig(plotfile, dpi=300)
        plt.close()

        n = rg['sizes']
        # plt.figure(figsize=(5, 5))
        plt.loglog(rg['sizes'], rg['l1'], '--', label='SPH')
        plt.loglog(rl3['sizes'], rl3['l1'], '.-', label=r'$\Lambda_3$')
        plt.loglog(rm4['sizes'], rm4['l1'], ':', label=r'$M_4$')
        plt.loglog(n, 5.0/(n*n), 'k:', label=r'$O(h^2)$')
        plt.legend()
        plt.grid()
        plt.xlabel('N')
        plt.ylabel(r'$L_1$')
        plt.title(r'$L_1(u-u_c)$')

        plotfile = self.output_path('l1_hdx1.png')
        plt.savefig(plotfile, dpi=300)
        plt.close()

    def plot_hdx_1_5(self):
        import code.remesh as remesh
        import matplotlib.pyplot as plt
        g = remesh.QuinticSpline(dim=2)
        l3_eqs = [remesh.Lambda3(dest='interpolate', sources=['src'])]
        m4_eqs = [remesh.M4(dest='interpolate', sources=['src'])]

        rg_m = remesh.moment_errors(g, 1.2)
        rl3_m = remesh.moment_errors(g, 1.2, equations=l3_eqs)
        rm4_m = remesh.moment_errors(g, 1.2, equations=m4_eqs)

        rg = remesh.find_error(g, 1.2)
        rl3 = remesh.find_error(g, 1.2, equations=l3_eqs)
        rm4 = remesh.find_error(g, 1.2, equations=m4_eqs)

        m = 'm2'
        # plt.figure(figsize=(5, 5))
        plt.loglog(rg_m['sizes'], rg_m['moment'][m], '--', label='SPH')
        plt.loglog(rl3_m['sizes'], rl3_m['moment'][m], '.-', label=r'$\Lambda_3$')
        plt.loglog(rm4_m['sizes'], rm4_m['moment'][m], ':', label=r'$M_4$')
        plt.legend()
        plt.grid()
        plt.xlabel('N')
        plt.ylabel(r'$M_2$')
        plt.title(r'$\sum u (x^2 + y^2) dx^2)$')

        plotfile = self.output_path('hdx1_5.png')
        plt.savefig(plotfile, dpi=300)
        plt.close()

        # plt.figure(figsize=(5, 5))
        n = rg['sizes']
        plt.loglog(rg['sizes'], rg['l1'], '--', label='SPH')
        plt.loglog(rl3['sizes'], rl3['l1'], '.-', label=r'$\Lambda_3$')
        plt.loglog(rm4['sizes'], rm4['l1'], ':', label=r'$M_4$')
        plt.loglog(n, 5.0/(n*n), 'k:', label=r'$O(h^2)$')
        plt.legend()
        plt.grid()
        plt.xlabel('N')
        plt.ylabel(r'$L_1$')
        plt.title(r'$L_1(u-u_c)$')

        plotfile = self.output_path('l1_hdx1_5.png')
        plt.savefig(plotfile, dpi=300)
        plt.close()


class StandardSPHApprox(SPHApproxBase):
    def get_name(self):
        return 'standard_sph'

    def params(self):
        self.kernels = ['QuinticSpline', 'CubicSpline', 'Gaussian', 'WendlandQuinticC6']
        self.radius = np.array([3, 2, 3, 2])/3
        self.hdx = [1.0, 1.2, 1.5, 1.7, 2.0, 2.5]
        self.derv = [0, 1]
        self.perturb = [0, 1]
        self.err = [1e-3]
        self.dim = 2
        self.N = RESOLUTIONS #[100, 200, 400]
        self.methods = ['sph']

    def setup(self):
        self.params()
        cmd = 'python ' + self._get_file() + ' --openmp '

        get_path = self.input_path
        self.case_info = {}
        for i, kernel in enumerate(self.kernels):
            case_info = {
                f'{m}_{kernel}_{N}_d{derv}_p{pert}_{hdx/self.radius[i]:.2f}_{max_err:.6f}':
                dict(kernel=kernel,
                    N=N,
                    perturb=pert,
                    hdx=hdx/self.radius[i],
                    derv=derv,
                    dim=self.dim,
                    use_sph=m,
                    max_err=max_err)
                for m, N, pert, hdx, derv, max_err in product(
                    self.methods, self.N, self.perturb, self.hdx,
                    self.derv, self.err)
            }
            self.case_info.update(case_info)

        self.cases = [
            Simulation(
                get_path(name), cmd, job_info=dict(n_core=1, n_thread=1),
                cache_nnps=None, **scheme_opts(Kwargs)
            ) for name, Kwargs in self.case_info.items()
        ]


        for case in self.cases:
            self.case_info[case.name]['case'] = case

    def _ylabel(self, error):
        if error == 'l1':
            return r'$L_1$'
        elif error == 'l2':
            return r'$L_2$'
        elif error == 'linf':
            return r'$L_{\infty}$'

    def run(self):
        self.make_output_dir()
        self.read_data()
        self.norm_all('l1')
        self.plot_particles()

    def _get_label(self, method, kernel, derv, N, pert, hdx=None, err=5e-4):
        return r'$h_{\Delta s}=%.2f$'%hdx

    def plot_particles(self):
        from pysph.solver.utils import get_files, load
        cases = filter_cases(self.cases, perturb=0, N=50)
        filepath = cases[0].input_path()
        files = get_files(filepath)
        data = load(files[0])

        print(files, data)
        fluid = data['arrays']['src']
        x = fluid.x
        y = fluid.y

        plt.scatter(x, y, s=2)
        plt.xlabel('x')
        plt.ylabel('y')
        plt.title('Unperturbed')
        filename = self.output_path('up.png')
        plt.savefig(filename, dpi=300)
        plt.close()

        cases = filter_cases(self.cases, perturb=1, N=50)
        filepath = cases[0].input_path()
        files = get_files(filepath)
        data = load(files[0])

        fluid = data['arrays']['src']
        x = fluid.x
        y = fluid.y

        plt.scatter(x, y, s=2)
        plt.xlabel('x')
        plt.ylabel('y')
        plt.title('Perturbed')
        filename = self.output_path('pp.png')
        plt.savefig(filename, dpi=300)
        plt.close()

    def norm_all(self, error='l1'):
        import matplotlib.pyplot as plt
        for perturb in self.perturb:
            for derv in self.derv:
                fig, axes = plt.subplots(2, 2, sharey=True, sharex=True, figsize=(10, 10))
                axes = np.ravel(axes)
                for i, kernel in enumerate(self.kernels):
                    marker = cycle(MARKER)
                    reset_cycle(marker, MARKER)
                    for err in self.err:
                        for method in self.methods:
                            for hdx in self.hdx:
                                _hdx = hdx/self.radius[i]
                                l2 = []
                                ns = []
                                time = []
                                for N in self.N:
                                    name = self.get_data_name(
                                        method, kernel, derv, N, perturb,
                                        hdx=_hdx, err=err)
                                    l2.append(self.data[name][error])
                                    time.append(self.data[name]['t'])
                                    ns.append(N)
                                _marker = next(marker)
                                axes[i].loglog(ns,
                                        l2,
                                        marker=_marker,
                                        label=self._get_label(method, kernel, derv, N, perturb, _hdx, err))

                    ns = np.array(ns)
                    # axes[i].ylim([1e-5, 1e-1])
                    axes[i].loglog(
                        ns, 0.1*l2[0] * (ns[0]/ns)**2, '--k', linewidth=1)
                    if (i == 2) or (i == 3):
                        axes[i].set_xlabel('N')
                    if (i == 0) or (i == 2):
                        axes[i].set_ylabel(self._ylabel(error))
                    axes[i].text(0.5, 0.95, kernel, transform=axes[i].transAxes)
                    axes[i].grid()
                    axes[i].legend(loc='lower left')

                plt.tight_layout()
                plotfile = self.output_path(
                    self.get_name() + '_derv%d_pert%d.png' %
                    (derv, perturb))
                plt.savefig(plotfile, dpi=300)
                plt.close()


class CorrectedSPHApprox(StandardSPHApprox):
    def get_name(self):
        return 'corr_sph'

    def params(self):
        self.kernels = ['QuinticSpline', 'CubicSpline', 'Gaussian', 'WendlandQuinticC6']
        self.radius = np.array([3, 2, 3, 2])/3
        self.hdx = [1.2]
        self.derv = [1]
        self.perturb = [1]
        self.err = [1e-3]
        self.dim = 2
        self.N = RESOLUTIONS
        self.methods = ['kc']

    def run(self):
        self.make_output_dir()
        self.read_data()
        self.norm_all('l1')

    def norm_all(self, error='l1'):
        import matplotlib.pyplot as plt
        derv = self.derv[0]
        perturb = self.perturb[0]
        err = self.err[0]
        hdx = self.hdx[0]
        method = self.methods[0]
        marker = cycle(MARKER)
        reset_cycle(marker, MARKER)
        for i, kernel in enumerate(self.kernels):
            _hdx = hdx/self.radius[i]
            l2 = []
            ns = []
            time = []
            for N in self.N:
                name = self.get_data_name(
                    method, kernel, derv, N, perturb,
                    hdx=_hdx, err=err)
                l2.append(self.data[name][error])
                time.append(self.data[name]['t'])
                ns.append(N)
            _marker = next(marker)
            plt.loglog(ns,
                    l2,
                    marker=_marker,
                    label=get_kernel_label(kernel))

        ns = np.array(ns)
        plt.loglog(
            ns, 0.8*l2[0] * (ns[0]/ns)**2, '--k', linewidth=1)
        plt.xlabel('N')
        plt.ylabel(self._ylabel(error))
        plt.grid()
        plt.legend(loc='lower left')

        plt.tight_layout()
        plotfile = self.output_path(
            self.get_name() + '_conv.png')
        plt.savefig(plotfile, dpi=300)
        plt.close()



class GradientApprox(StandardSPHApprox):
    def get_name(self):
        return 'grad_sph'

    def run(self):
        self.make_output_dir()
        self.read_data()
        self.norm_all('l1')
        self.table_all()
        self.remove_data()

    def remove_data(self):
        if CLEAN:
            import glob
            import os
            for case in self.cases:
                dirname = case.input_path()
                path = os.path.join(dirname , 'equation_convergence*.hdf5')
                names0 = glob.glob(path)
                path = os.path.join(dirname , 'equation_convergence*.npz')
                names1 = glob.glob(path)
                names = names0 + names1
                for name in names:
                    print("removing", name)
                    os.remove(name)

    def params(self):
        self.kernels = ['QuinticSpline']
        self.hdx = [1.2]
        self.derv = [1]
        self.perturb = [0, 1]
        self.radius = [1]
        self.err = [1e-3]
        self.dim = 2
        self.N = RESOLUTIONS #[100, 200, 400]
        self.methods = ['me', 'me_bonet', 'me_liu', 'me_pji', 'me_pji_bonet', 'me_symm', 'me_symm_bonet', 'me_symm_liu', 'me_mc']

    def _get_file(self):
        return 'code/equation_convergence.py'

    def _get_label(self, method, kernel, derv, N, pert, hdx=None, err=5e-4):
        if method == 'me':
            return 'sym2'
        elif method == 'me_bonet':
            return 'sym2_bc'
        elif method == 'me_liu':
            return 'sym2_lc'
        elif method == 'me_symm':
            return 'sym1'
        elif method == 'me_symm_bonet':
            return 'sym1_bc'
        elif method == 'me_symm_liu':
            return 'sym1_lc'
        elif method == 'me_pji':
            return 'asym'
        elif method == 'me_pji_bonet':
            return 'asym_bc'
        elif method == 'me_mc':
            return 'sym_sl'

    def _set_ylim(self, plt):
        plt.ylim([1e-5, 1e-1])

    def _set_title(self, plt, perturb):
        if perturb:
            plt.title('Perturbed')
        else:
            plt.title('Unperturbed')

    def _set_legend(self, plt):
        plt.legend()

    def _get_order(self, method, kernel, derv, perturb, hdx, err):
        nx = RESOLUTIONS
        l1 = []
        for N in nx:
            name = self.get_data_name(
                method, kernel, derv, N, perturb, hdx=hdx, err=err)
            l1.append(self.data[name]['l1'])
        l1 = np.array(np.log(l1))
        nx = np.array(np.log(nx), dtype=float)
        # nx = 1/nx

        # obtaining linear regression coefficient
        # https://www.geeksforgeeks.org/linear-regression-python-implementation/
        n = np.size(nx)

        # mean of x and y vector
        m_x = np.mean(nx)
        m_y = np.mean(l1)

        # calculating cross-deviation and deviation about x
        SS_xy = np.sum(l1 * nx) - n * m_y * m_x
        SS_xx = np.sum(nx * nx) - n * m_x * m_x

        # calculating regression coefficients
        order = -SS_xy / SS_xx
        return order

    def table_all(self):
        for perturb in self.perturb:
            for derv in self.derv:
                for kernel in self.kernels:
                    for err in self.err:
                        for hdx in self.hdx:
                            cols = ['Name', r'$\frac{F_T}{F_{max}}$', r'$T_{r}$', r'$L_1$ error', 'Order']
                            format = 'cllll'
                            N = 500
                            rows = []
                            mintime = 1000.0
                            for method in self.methods:
                                name = self.get_data_name(
                                        method, kernel, derv, N, perturb,
                                        hdx=hdx, err=err)
                                label = self._get_label(method, kernel, derv, N, perturb, hdx, err)
                                tmp = label.split('_')
                                if len(tmp) > 1:
                                    label = tmp[0] + '\_' + tmp[1]
                                force = self.data[name]['F']
                                time = self.data[name]['t']
                                l1 = self.data[name]['l1']
                                order = self._get_order(
                                        method, kernel, derv, perturb,
                                        hdx, err)
                                if time < mintime:
                                    mintime = time
                                rows.append([r'\nam{%s}'%label, '%.2e'%force, time, '%.2e'%l1, '%.2f'%order ])
                            tabfile = self.output_path(
                                '%s_derv%d_pert%d.tex' %
                                (kernel, derv, perturb))
                            for row in rows:
                                row[-3] /= mintime
                                row[-3] = '%.2f'%row[-3]
                            make_table(cols, rows, tabfile, column_format=format, sort_cols=[0])

    def norm_all(self, error='l1'):
        import matplotlib.pyplot as plt

        fig, axes = plt.subplots(1, 2, sharey=True, figsize=(10, 5))
        derv = 1
        kernel = self.kernels[0]
        err = self.err[0]
        hdx = self.hdx[0]
        for i, perturb in enumerate(self.perturb):
            marker = cycle(MARKER)
            reset_cycle(marker, MARKER)
            for method in self.methods:
                l2 = []
                ns = []
                time = []
                for N in self.N:
                    name = self.get_data_name(
                        method, kernel, derv, N, perturb,
                        hdx=hdx, err=err)
                    l2.append(self.data[name][error])
                    time.append(self.data[name]['t'])
                    ns.append(hdx * 1/N)
                _marker = next(marker)
                axes[i].loglog(ns,
                        l2, '--',
                        marker=_marker,  mfc='none',
                        label=self._get_label(method, kernel, derv, N, perturb, hdx, err))
            ns = np.array(ns)
            axes[i].loglog(
                ns, 0.5*l2[0] * (ns/ns[0])**2, '--k', linewidth=1)
            axes[i].set_xlabel('h')
            if i ==0:
                axes[i].set_ylabel(self._ylabel(error))
            if perturb == 0:
                axes[i].text(0.8, 0.05, 'Unpertubed', transform=axes[i].transAxes)
            elif perturb == 1:
                axes[i].text(0.8, 0.05, 'Pertubed', transform=axes[i].transAxes)
            axes[i].grid()
        handles, labels = axes[0].get_legend_handles_labels()
        # Comment this block if texlive is not installed
        from distutils.spawn import find_executable
        if find_executable('latex'):
            plt.rc('text', usetex=True)
            for i, l in enumerate(labels):
                tmp = l.split('_')
                if len(tmp) > 1:
                    labels[i] = tmp[0] + '\_' + tmp[1]
            labels = [r"\texttt{%s}"%l for l in labels]
            print(labels)
        fig.legend(handles, labels, bbox_to_anchor=[0.5, 0.92], ncol=5, loc='center')
        plt.tight_layout()
        plt.subplots_adjust(top=0.83, wspace=0.0)
        plotfile = self.output_path(self.get_name() + '_convergence.png')
        plt.savefig(plotfile, dpi=300)
        plt.close(1)


class DivergenceApprox(GradientApprox):
    def get_name(self):
        return 'div_sph'

    def _get_label(self, method, kernel, derv, N, pert, hdx=None, err=5e-4):
        if method == 'ce':
            return 'div'
        elif method == 'ce_bonet':
            return 'div_bc'

    def params(self):
        self.kernels = ['QuinticSpline']
        self.hdx = [1.2]
        self.derv = [1]
        self.radius = [1]
        self.perturb = [0, 1]
        self.err = [1e-3]
        self.dim = 2
        self.N = RESOLUTIONS
        self.methods = ['ce', 'ce_bonet']

class DivergenceApproxZeroDiv(DivergenceApprox):
    def get_name(self):
        return 'div_sph_zero_div'

    def _get_file(self):
        return 'code/equation_convergence.py --zero-div y '

class LaplaceApprox(GradientApprox):
    def get_name(self):
        return 'laplace_sph'

    def _set_legend(self, plt):
        plt.legend(ncol = 3)

    def _get_label(self, method, kernel, derv, N, pert, hdx=None, err=5e-4):
        if method == 'visc':
            return 'tvf'
        elif method == 'visc_bonet':
            return 'tvf_bc'
        elif method == 'visc_liu':
            return 'tvf_lc'
        elif method == 'visc_coupled':
            return 'coupled'
        elif method == 'visc_coupled_bonet':
            return 'coupled_bc'
        elif method == 'visc_cleary':
            return 'Cleary'
        elif method == 'visc_cleary_bonet':
            return 'Cleary_bc'
        elif method == 'visc_cleary_liu':
            return 'Cleary_lc'
        elif method == 'visc_fatehi':
            return 'Fatehi'
        elif method == 'visc_fatehi_fatehi':
            return 'Fatehi_c'
        elif method == 'visc_fatehi_bonet':
            return 'Fatehi_bc'
        elif method == 'visc_fatehi_liu':
            return 'Fatehi_lc'
        elif method == 'visc_korzilius':
            return 'Korzilius'

    def params(self):
        self.kernels = ['QuinticSpline']
        self.hdx = [1.2]
        self.derv = [1]
        self.perturb = [0, 1]
        self.radius = [1]
        self.err = [1e-3]
        self.dim = 2
        self.N = RESOLUTIONS
        self.methods = [
            'visc', 'visc_bonet', 'visc_liu', 'visc_coupled',
            'visc_coupled_bonet', 'visc_cleary', 'visc_cleary_bonet',
            'visc_cleary_liu', 'visc_fatehi', 'visc_fatehi_fatehi',
            'visc_korzilius'
        ]


class LaplaceCompare(StandardSPHApprox):
    def get_name(self):
        return 'laplace_compare'

    def _get_file(self):
        return 'code/heat_diffusion.py --max-step 2000'

    def params(self):
        self.kernels = ['QuinticSpline']
        self.hdx = [1.2]
        self.derv = [1]
        self.perturb = [0, 1]
        self.radius = [1]
        self.dim = 1
        self.err = [1e-3]
        self.N = [50, 500]
        self.methods = [
            'visc_coupled_bonet',
            'visc_fatehi_fatehi',
            'visc_coupled_bonet --do-post 1 '
        ]

    def run(self):
        self.make_output_dir()
        self.plot_all()

    def _get_label(self, case):
        method = case.params['use_sph']
        if method == 'visc_coupled_bonet':
            return 'coupled_c'
        elif method == 'visc_fatehi_fatehi':
            return 'fatehi_f'
        else:
            return 'coupled_c smoothed'

    def plot_all(self):
        N = self.N
        pert = self.perturb

        for n in N:
            for p in pert:
                cases = filter_cases(self.cases, N=n, perturb=p)
                plt.figure()
                for case in cases:
                    data = case.data
                    x = data['x']
                    u = data['u']
                    label = self._get_label(case)
                    plt.plot(x, u, '--', label=label)
                plt.xlabel('x')
                plt.ylabel('T')
                plt.legend()
                plt.xlim([0.25, 0.75])
                plt.grid()
                filename = self.output_path( '%d_%d_pert.png' %(n, p))
                plt.savefig(filename, dpi=300)
                plt.close()


class SD(SPHApproxBase):
    def get_name(self):
        return 'sd_analysis'

    def params(self):
        self.kernels = KERNELS
        self.N = [100, 100, 200, 200, 400, 400, 800, 800, 1000, 1000]
        self.nnbr = [30, 40, 50, 70, 100, 130, 160, 190, 210, 240]
        self.methods = ['sd']

    def setup(self):
        from pysph.base import kernels
        self.params()
        cmd = 'python ' + self._get_file()

        self.hdx = np.ones_like(self.nnbr)
        get_path = self.input_path
        self.case_info = {}
        index = 0
        for m, kernel in product(self.methods, self.kernels):
            index = 0
            kernel_cls = getattr(kernels, kernel)(dim=2)
            rad = kernel_cls.radius_scale
            for i, N in enumerate(self.N):
                self.hdx[i] = hdx = np.sqrt(self.nnbr[i] / (rad**2 * np.pi))
                self.case_info.update({
                    f'{m}_{kernel}_{N}_{self.nnbr[i]}':
                    dict(kernel=kernel,
                         N=N,
                         dim=2,
                         use_sph=m,
                         hdx=hdx,
                         derv=self.nnbr[i])
                })
                index += 1

        self.cases = [
            Simulation(
                get_path(name), cmd, job_info=dict(n_core=2, n_thread=16),
                cache_nnps=None, **scheme_opts(Kwargs)
            ) for name, Kwargs in self.case_info.items()
        ]

        for case in self.cases:
            self.case_info[case.name]['case'] = case

    def run(self):
        self.make_output_dir()
        self.plot_all_kernel()

    def _get_label(self, kernel):
        return get_kernel_label(kernel)

    def plot_all_kernel(self):
        import matplotlib.pyplot as plt
        linestyle = cycle(LINESTYLE)
        marker = cycle(MARKER)
        for kernel in self.kernels:
            rho = []
            nnbr = []
            for _nnbr in self.nnbr:
                cases = filter_cases(self.cases, kernel=kernel, derv=_nnbr)
                data = cases[0].data
                rho.append(data['rho'][50])
                nnbr.append(data['nnbr'][50])

            _marker = next(marker)
            _ls = next(linestyle)
            plt.semilogy(nnbr,
                         abs(np.array(rho) - 1.0),
                         label=self._get_label(kernel),
                         marker=_marker,
                         linestyle=_ls)
        plt.legend()
        plt.grid()
        plt.ylabel(r'$L_1$ error')
        plt.xlabel(r'$N_{nbr}$')
        plotfile = self.output_path('rho.png')
        plt.savefig(plotfile, dpi=300)
        plt.close()


class PoiseuilleFlow(PySPHProblem):
    def get_name(self):
        return 'poiseuille'

    def _get_file(self):
        return 'code/poiseuille.py --openmp --tf 10 --pst-freq 10 '

    def setup(self):
        self.schemes = ['edac', 'tvf', 'tsph']
        self.res = [20, 40, 80] #RESOLUTIONS
        cmd = 'python ' + self._get_file()

        get_path = self.input_path
        self.case_info = {}
        index = 0
        for scheme in self.schemes:
            for res in self.res:
                self.case_info.update({
                    f'{scheme}_{res}':
                    dict(nx=res,
                         scheme=scheme,
                         timestep=0.000195313)
                })

        self.cases = [
            Simulation(
                get_path(name), cmd, job_info=dict(n_core=2, n_thread=8),
                cache_nnps=None, **scheme_opts(Kwargs)
            ) for name, Kwargs in self.case_info.items()
        ]

        for case in self.cases:
            self.case_info[case.name]['case'] = case

    def remove_data(self):
        if CLEAN:
            import glob
            import os
            for case in self.cases:
                dirname = case.input_path()
                path = os.path.join(dirname , 'poiseuille*.hdf5')
                names0 = glob.glob(path)
                path = os.path.join(dirname , 'poiseuille*.npz')
                names1 = glob.glob(path)
                names = names0 + names1
                for name in names:
                    print("removing", name)
                    os.remove(name)

    def run(self):
        self.make_output_dir()
        self._plot_convergence()
        self.remove_data()

    def calculate_l1(self, cases):
        data = {}
        for case in cases:
            u = case.data['u']
            ue = case.data['u_ex']
            l1 = sum(abs(u - ue))/sum(abs(ue))
            data[case.params['nx']] = l1
        nx_arr = np.asarray(sorted(data.keys()), dtype=float)
        l1 = np.asarray([data[x] for x in nx_arr])
        return nx_arr, l1

    def _plot_convergence(self):
        plt.figure()
        dx = None
        marker = cycle(MARKER)
        for scheme in self.schemes:
            cases = filter_cases(self.cases, scheme=scheme)
            nx, l1 = self.calculate_l1(cases)
            dx = 1.0/nx * 1.2
            plt.loglog(dx, l1, label=get_label_from_scheme(scheme), marker=next(marker))

        plt.loglog(dx, l1[0]*(dx/dx[0])**2, 'k--', linewidth=2,
                label=r'$O(h^2)$')
        plt.loglog(dx, l1[0]*(dx/dx[0]), 'k:', linewidth=2,
                label=r'$O(h)$')
        plt.grid()
        plt.legend(loc='best')
        plt.xlabel(r'$h$')
        plt.ylabel(r'$L_1$ error')
        plt.savefig(self.output_path('pois_conv.png'), dpi=300)
        plt.close()


class TaylorGreenVarTimestep(PoiseuilleFlow):
    def get_name(self):
        return 'tg_var_dt'

    def _get_file(self):
        return 'code/taylor_green.py --openmp --tf 0.2 --pst-freq 10 '

    def setup(self):
        scheme_opts = mdict(scheme=['tsph'], re=[100, 1000, 5000])
        integrator_opts = mdict(
            integrator=['pec'], integrator_dt_mul_fac=[1, 2]
        )
        integrator_opts += mdict(
            integrator=['rk2'], integrator_dt_mul_fac=[2, 4]
        )
        integrator_opts += mdict(
            integrator=['rk3'], integrator_dt_mul_fac=[3, 6]
        )
        integrator_opts += mdict(
            integrator=['rk4'], integrator_dt_mul_fac=[4, 8]
        )
        res_opts = mdict(nx=[25, 50, 100, 200, 400, 800], c0_fac=[40, 80])
        sim_opts = dprod(scheme_opts, dprod(integrator_opts, res_opts))
        self.sim_opts = sim_opts

        # Unpack the simulation options
        self.schemes = get_all_unique_values(sim_opts, 'scheme')
        self.re_s = get_all_unique_values(sim_opts, 're')
        self.integrators = get_all_unique_values(sim_opts, 'integrator')
        self.nx = get_all_unique_values(sim_opts, 'nx')
        self.dt_mul_facs = get_all_unique_values(
            sim_opts, 'integrator_dt_mul_fac'
        )
        self.c0s = get_all_unique_values(sim_opts, 'c0_fac')

        cmd = 'python ' + self._get_file()
        get_path = self.input_path
        self.case_info = {}
        for i in range(len(sim_opts)):
            sim_name = opts2path(
                sim_opts[i],
                kmap=dict(integrator_dt_mul_fac='dtmul', c0_fac='c0')
            )
            self.case_info[sim_name] = sim_opts[i]

        self.cases = [
            Simulation(
                get_path(name), cmd, job_info=dict(n_core=2, n_thread=2),
                cache_nnps=None, **Kwargs
            ) for name, Kwargs in self.case_info.items()
        ]

        for case in self.cases:
            self.case_info[case.name]['case'] = case

    def remove_data(self):
        if CLEAN:
            import glob
            import os
            for case in self.cases:
                dirname = case.input_path()
                path = os.path.join(dirname , 'taylor_green*.hdf5')
                names0 = glob.glob(path)
                path = os.path.join(dirname , 'taylor_green*.npz')
                names1 = glob.glob(path)
                names = names0 + names1
                for name in names:
                    print("removing", name)
                    os.remove(name)

    def run(self):
        self.make_output_dir()
        for c0 in self.c0s:
            for re in self.re_s:
                self._plot_convergence(c0=c0, re=re)
        for intg in self.integrators:
            for re in self.re_s:
                if intg == 'auto':
                    continue
                self._plot_convergence_c0(intg=intg, re=re)
        for re in self.re_s:
            self._plot_rt_speedup(re=re, largest_dtmf_only=True)
            self._plot_rt_speedup(re=re, largest_dtmf_only=False)
        self.remove_data()

    def calculate_l1(self, cases):
        data = {}
        for case in cases:
            l1 = case.data['l1']
            l1 = sum(l1)/len(l1)
            data[case.params['nx']] = l1
        nx_arr = np.asarray(sorted(data.keys()), dtype=float)
        l1 = np.asarray([data[x] for x in nx_arr])
        return nx_arr, l1

    def _plot_rt_speedup(self, c0=None, re=None, largest_dtmf_only=True):
        if c0 is None:
            c0 = max(self.c0s)
        if re is None:
            re = max(self.re_s)

        plt.figure()
        dx = None
        marker = cycle(MARKER)
        scheme = 'tsph'
        for intg in self.integrators:
            if intg == 'auto':
                continue
            if largest_dtmf_only:
                temp = get_all_unique_values(
                    self.sim_opts, 'integrator_dt_mul_fac',
                    option=dict(integrator=intg)
                )
                if intg == 'pec':
                    r_list = [1, max(temp)]
                else:
                    r_list = [max(temp)]
            else:
                r_list = self.dt_mul_facs
            for dtmf in r_list:
                rts = []
                dts = []
                for dt in self.nx:
                    cases = filter_cases(
                        self.cases, scheme=scheme, integrator=intg, nx=dt,
                        c0_fac=c0, integrator_dt_mul_fac=dtmf, re=re
                    )
                    if len(cases) < 1:
                        continue
                    case = cases[0]
                    rts.append(get_cpu_time(case))
                    dts.append(case.params['nx'])
                if len(rts) < 1:
                    continue
                label = fr'{intg} ({dtmf}$\Delta t$)'
                plt.plot(dts, rts, label=label, marker=next(marker))


        plt.grid()
        plt.legend(loc='best')
        plt.xlabel(r'$N_x$')
        plt.ylabel(r'$RT$')
        plt.title(f'CPU time - L-IPST-C Scheme (c0={c0}) (Re={re})')
        if largest_dtmf_only:
            fname = f'pois_rt_re_{re}_largest_dtmf.png'
        else:
            fname = f'pois_rt_re_{re}.png'
        plt.savefig(self.output_path(fname), dpi=300)

    def _plot_convergence(self, c0=None, re=None):
        if c0 is None:
            c0 = min(self.c0s)
        if re is None:
            re = max(self.re_s)
        plt.figure()
        marker = cycle(MARKER)
        for scheme in self.schemes:
            for intg in self.integrators:
                for dtmf in self.dt_mul_facs:
                    cases = filter_cases(
                        self.cases, scheme=scheme, integrator=intg, c0_fac=c0,
                        integrator_dt_mul_fac=dtmf, re=re
                    )
                    if len(cases) < 1:
                        continue
                    dts, l1 = self.calculate_l1(cases)
                    dts = 1./dts
                    label = get_label_from_scheme(scheme) +\
                         fr' ({intg}) ({dtmf}$\Delta t$)'
                    plt.loglog(dts, l1, label=label, marker=next(marker))

        plt.loglog(dts, l1[0]*(dts/dts[0])**2, 'k--', linewidth=2,
                label=r'$O(h^2)$')
        plt.loglog(dts, l1[0]*(dts/dts[0]), 'k:', linewidth=2,
                label=r'$O(h)$')
        plt.grid()
        plt.legend(loc='best')
        plt.xlabel(r'$h$')
        plt.ylabel(r'$L_1$ error')
        plt.title(fr'Scheme convergence - $L_1$ error $(c_0={c0})$ (Re={re})')
        plt.savefig(
            self.output_path(f'dt_pois_conv_c0_{c0}_re_{re}.png'), dpi=300
        )
        plt.close()

    def _plot_convergence_c0(self, intg, re=None):
        if re is None:
            re = max(self.re_s)
        plt.figure()
        marker = cycle(MARKER)
        scheme = 'tsph'
        if intg not in self.integrators:
            return
        for c0 in self.c0s:
            for dtmf in self.dt_mul_facs:
                cases = filter_cases(
                    self.cases, scheme=scheme, integrator=intg, c0_fac=c0,
                    integrator_dt_mul_fac=dtmf, re=re
                )
                if len(cases) < 1:
                    continue
                dts, l1 = self.calculate_l1(cases)
                dts = 1./dts
                label = get_label_from_scheme(scheme) +\
                    fr' ({intg}) ({dtmf}$\Delta t$)'
                label += f' (c0={c0})'
                plt.loglog(dts, l1, label=label, marker=next(marker))
        
        plt.loglog(dts, l1[0]*(dts/dts[0])**2, 'k--', linewidth=2,
                label=r'$O(h^2)$')
        plt.loglog(dts, l1[0]*(dts/dts[0]), 'k:', linewidth=2,
                label=r'$O(h)$')
        plt.grid()
        plt.legend(loc='best')
        plt.xlabel(r'$h$')
        plt.ylabel(r'$L_1$ error')
        plt.title(fr'Scheme convergence - $L_1$ error (Re={re})')
        fname = self.output_path(f'dt_pois_conv_c0_{intg}_re_{re}.png')
        plt.savefig(fname, dpi=300)
        plt.close()


class PoiseuilleFlowLongtime(PySPHProblem):
    def get_name(self):
        return 'poiseuille_lt'

    def _get_file(self):
        return 'code/poiseuille.py --openmp --tf 100 --pfreq 1000 --nx 80 --pst-freq 10 '

    def setup(self):
        self.schemes = ['edac', 'tvf', 'tsph']
        cmd = 'python ' + self._get_file()

        get_path = self.input_path
        self.case_info = {}
        index = 0
        for scheme in self.schemes:
            self.case_info.update({
                f'{scheme}':
                dict(scheme=scheme)
            })

        self.cases = [
            Simulation(
                get_path(name), cmd, job_info=dict(n_core=2, n_thread=8),
                cache_nnps=None, **scheme_opts(Kwargs)
            ) for name, Kwargs in self.case_info.items()
        ]

        for case in self.cases:
            self.case_info[case.name]['case'] = case

    def remove_data(self):
        if CLEAN:
            import glob
            import os
            for case in self.cases:
                dirname = case.input_path()
                path = os.path.join(dirname , 'poiseuille*.hdf5')
                names0 = glob.glob(path)
                path = os.path.join(dirname , 'poiseuille*.npz')
                names1 = glob.glob(path)
                names = names0 + names1
                for name in names:
                    print("removing", name)
                    os.remove(name)

    def run(self):
        self.make_output_dir()
        self._plot_t_hist()
        self._plot_data()
        self.remove_data()

    def _plot_t_hist(self):
        plt.figure()
        dx = None
        marker = cycle(MARKER)
        for scheme in self.schemes:
            cases = filter_cases(self.cases, scheme=scheme)
            t, ke = cases[0].data['t'], cases[0].data['ke']
            plt.plot(t, ke, label=get_label_from_scheme(scheme))

        plt.grid()
        plt.legend(loc='best')
        plt.xlabel('t')
        plt.ylabel('kinetic energy')
        plt.savefig(self.output_path(self.get_name()+'_ke.png'), dpi=300)
        plt.close()

    def _plot_data(self):
        plt.figure()
        dx = None
        marker = cycle(MARKER)
        for scheme in self.schemes:
            cases = filter_cases(self.cases, scheme=scheme)
            t, u = cases[0].data['y'], cases[0].data['u']
            plt.plot(t, u, 'o', label=get_label_from_scheme(scheme), mfc='none')

        ye = np.linspace(0, 1.0, 100)
        ue = 0.5 * 128./16. * ye*(1. - ye)
        plt.plot(ye, ue, label='exact')
        plt.grid()
        plt.legend(loc='best')
        plt.xlabel('y')
        plt.ylabel(r'$|\mathbf{u}|$')
        plt.savefig(self.output_path(self.get_name() + '_u.png'), dpi=300)
        plt.close()



class TaylorGreenLongtime(PoiseuilleFlowLongtime):
    def get_name(self):
        return 'tg_lt'

    def _get_file(self):
        return 'code/taylor_green.py --openmp --tf 5.0 --pfreq 1000 --nx 100 --pst-freq 10 '

    def remove_data(self):
        if CLEAN:
            import glob
            import os
            for case in self.cases:
                dirname = case.input_path()
                path = os.path.join(dirname , 'taylor_green*.hdf5')
                names0 = glob.glob(path)
                path = os.path.join(dirname , 'taylor_green*.npz')
                names1 = glob.glob(path)
                names = names0 + names1
                for name in names:
                    print("removing", name)
                    os.remove(name)


    def _plot_data(self):
        plt.figure()
        t = None
        marker = cycle(MARKER)
        for scheme in self.schemes:
            cases = filter_cases(self.cases, scheme=scheme)
            t, d = cases[0].data['t'], cases[0].data['decay']
            plt.semilogy(t, d, label=get_label_from_scheme(scheme))

        decay_rate = -8.0 * np.pi**2 / 100.
        decay_ex = np.exp(decay_rate * t)
        plt.semilogy(t, decay_ex, label='exact')
        plt.grid()
        plt.legend(loc='best')
        plt.xlabel('t')
        plt.ylabel(r'$|\mathbf{u}|_{max}$')
        plt.savefig(self.output_path(self.get_name() + '_u.png'), dpi=300)
        plt.close()


class GreshoChanLongtime(PoiseuilleFlowLongtime):
    def get_name(self):
        return 'gc_lt'

    def _get_file(self):
        plt.ylabel('kinetic energy')
        plt.ylabel('kinetic energy')
        return 'code/gresho_chan.py --openmp --tf 7.0 --pfreq 1000 --nx 100 --pst-freq 10 --damp-pre '

    def remove_data(self):
        if CLEAN:
            import glob
            import os
            for case in self.cases:
                dirname = case.input_path()
                path = os.path.join(dirname , 'gresho_chan*.hdf5')
                names0 = glob.glob(path)
                path = os.path.join(dirname , 'gresho_chan*.npz')
                names1 = glob.glob(path)
                names = names0 + names1
                for name in names:
                    print("removing", name)
                    os.remove(name)

    def _plot_t_hist(self):
        plt.figure()
        dx = None
        marker = cycle(MARKER)
        for scheme in self.schemes:
            cases = filter_cases(self.cases, scheme=scheme)
            t, ke = cases[0].data['time'], cases[0].data['mom']
            plt.semilogy(t, abs(ke), label=get_label_from_scheme(scheme))

        plt.grid()
        plt.legend(loc='best')
        plt.xlabel('t')
        plt.ylabel('Linear momentum')
        plt.tight_layout(pad=0)
        plt.savefig(self.output_path(self.get_name()+'_lm.png'), dpi=300)
        plt.close()

        plt.figure()
        dx = None
        marker = cycle(MARKER)
        for scheme in self.schemes:
            cases = filter_cases(self.cases, scheme=scheme)
            t, ke = cases[0].data['time'], cases[0].data['a_mom']
            plt.semilogy(t, abs(ke), label=get_label_from_scheme(scheme))

        plt.grid()
        plt.legend(loc='best')
        plt.xlabel('t')
        plt.ylabel('Angular momentum')
        plt.tight_layout(pad=0)
        plt.savefig(self.output_path(self.get_name()+'_am.png'), dpi=300)
        plt.close()


    def _plot_data(self):
        plt.figure()
        xe, ve = None, None
        marker = cycle(MARKER)
        for scheme in self.schemes:
            cases = filter_cases(self.cases, scheme=scheme)
            data = cases[0].data
            r = data['r']
            vmag = data['vmag']
            xe = data['xe']
            ve = data['ve']
            plt.plot(r, vmag, 'o', label=get_label_from_scheme(scheme), mfc='none')

        plt.plot(xe, ve, '--', label="exact")
        plt.grid()
        plt.xlabel('r')
        plt.ylabel(r'$|\mathbf{u}|$')
        plt.xlim([0, 0.5])
        plt.legend()
        plt.title('time = 7s')
        plt.savefig(self.output_path(self.get_name() + '_u.png'), dpi=300)
        plt.close()


class Speedup(PySPHProblem):
    def get_name(self):
        return 'gc_speedup'

    def _get_file(self):
        return 'code/gresho_chan.py --openmp --max-step 100 --disable-output --nx 100 --pst-freq 10 --damp-pre --method nosoc '

    def setup(self):
        self.schemes = ['edac', 'tvf', 'tsph', 'delta_plus', 'ewcsph']
        self.threads = [1, 2, 4, 8, 16, 32]
        cmd = 'python ' + self._get_file()

        get_path = self.input_path
        self.case_info = {}
        self.cases = []
        for scheme in self.schemes:
            for th in self.threads:
                self.cases.append(
                    Simulation(
                        get_path(f'{scheme}_{th}'), cmd,
                        job_info=dict(n_core=-1, n_thread=th),
                        cache_nnps=None, scheme=scheme
                    )
                )

    def remove_data(self):
        if CLEAN:
            import glob
            import os
            for case in self.cases:
                dirname = case.input_path()
                path = os.path.join(dirname , 'gresho_chan_with_boundary*.hdf5')
                names0 = glob.glob(path)
                path = os.path.join(dirname , 'gresho_chan_with_boundary*.npz')
                names1 = glob.glob(path)
                names = names0 + names1
                for name in names:
                    print("removing", name)
                    os.remove(name)

    def run(self):
        self.make_output_dir()
        self._plot_speedup()
        self.remove_data()

    def _get_times(self, cases):
        data = {}
        for case in cases:
            time = get_cpu_time(case)
            data[case.job_info['n_thread']] = time
        th = np.asarray(sorted(data.keys()), dtype=float)
        times = np.asarray([data[x] for x in th])
        return th, times[0]/times

    def _plot_speedup(self):
        for scheme in self.schemes:
            cases = filter_cases(self.cases, scheme=scheme)
            th, times = self._get_times(cases)
            plt.plot(th, times, 'o-', label=get_label_from_scheme(scheme))

        plt.grid()
        plt.xlabel('Threads')
        plt.ylabel('Speed-up')
        plt.legend()
        plt.savefig(self.output_path(self.get_name() + '_speed_up.png'), dpi=300)
        plt.close()


# Tyrpes of kernels
KERNELS = [
    'Gaussian', 'CubicSpline', 'QuinticSpline', 'WendlandQuintic',
    'WendlandQuinticC4', 'WendlandQuinticC6'
]
KERNELS_1D = [
    'Gaussian', 'CubicSpline', 'QuinticSpline', 'WendlandQuinticC2_1D',
    'WendlandQuinticC4_1D', 'WendlandQuinticC6_1D']


if __name__ == '__main__':
    # Note that we add all the problems here so that we can run particular
    # problems if desired.
    from tg_automation import (
        ComparisonSchemes, ComparisonC0, ComparisonVariation,
        ComparisonCostAll
                              )
    from gc_automation import ComparisonSchemesGC, ComparisonVariationGC, ComparisonVariationGCNW
    from sl_automation import ComparisonSchemesSL, ComparisonVariationSL
    all_problems = [
        RemeshingEquations,
        StandardSPHApprox, SD,
        CorrectedSPHApprox,
        GradientApprox,
        DivergenceApprox,
        DivergenceApproxZeroDiv,
        LaplaceApprox,
        LaplaceCompare,
        ComparisonSchemes,
        ComparisonC0,
        ComparisonVariation,
        ComparisonSchemesGC,
        ComparisonVariationGC,
        ComparisonSchemesSL,
        ComparisonVariationSL,
        ComparisonCostAll,
        ComparisonVariationGCNW,
        PoiseuilleFlow,
        TaylorGreenVarTimestep,
        PoiseuilleFlowLongtime,
        TaylorGreenLongtime,
        GreshoChanLongtime,
        Speedup,
    ]
    automator = Automator(
        simulation_dir='outputs',
        output_dir=os.path.join('manuscript', 'figures'),
        all_problems=all_problems
    )
    automator.run()
